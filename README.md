# Tesla Frontend Interview Assignment

This repo contains the brief, code and artifacts for a hypothetical Tesla Model S website, the frontend interview assignment for SendCloud. It can be viewed live at [tesla.vanhattum.xyz](demo).

It also now contains a rather long README.

## Getting Started

This project requires [Node](node) and a compatible package manager. It was developed using `node@18.0` and `npm@8.6`, but `yarn` or `pnpm` should work fine as well.

This project is a static website built in in HTML, [SCSS](scss) and [TypeScript](typescript). Bundling, image optimization and transpilation are handled by [Parcel](parcel).

To run the project:

- Install dependencies (`npm i`)
- Run `npm run start` to start the local development server.
- Go to http://localhost:1234 to view the site.

To build the project:

- Install dependencies (`npm i`).
- Run `npm run build`.
- The built artifacts (HTML, CSS, JS, and images) can be found in `./dist`.

To deploy the project:

- Install dependencies (`npm i`).
- Install the Firebase CLI (`npm install -g firebase-tools`).
- Run `npm run deploy`.

## Deployment

The project is deployed using [Google Firebase](firebase). Firebase is easy to use and uses GCP's CDN network around the world, resulting in a pleasant experience for both developers and users.

> Firebase is an app development platform that helps you build and grow apps and games users love. Backed by Google and trusted by millions of businesses around the world.

Deploying to Firebase is done via the [Firebase CLI](firebase-cli) by executing `firebase deploy`. Running `npm run deploy` builds the project and deploys it to Firebase.

Configuration can found in `firebase.json` and `.firebaserc`.

## Technologies

### [TypeScript](typescript)

> TypeScript is a strongly typed programming language that builds on JavaScript, giving you better tooling at any scale.

TypeScript provides strong(ish) typing for JavaScript, preventing many developer mistakes at compile time (and providing some nifty editor autocompletions as well).

### [SCSS](scss)

> Sass is a stylesheet language that’s compiled to CSS. It allows you to use variables, nested rules, mixins, functions, and more, all with a fully CSS-compatible syntax. Sass helps keep large stylesheets well-organized and makes it easy to share design within and across projects.

### [Parcel](parcel)

> Parcel combines a great out-of-the-box development experience with a scalable architecture that can take your project from just getting started to massive production application.

In this project, Parcel

- compiles TypeScript to JavaScript;
- compiles, bundles SCSS;
- provides backwards-compatibility for older browsers;
- and finally optimizes and resizes images.

Alternatives to Parcel would be Webpack, Babel, Rollup, Gulp, Grunt and others, but I chose Parcel because it of it's many functionalities and minimal required configuration.

## Accessibility

The following was done in order to maximise the accessibility of the site:

- Most sizes are defined in `rem` units, allowing users to increase the size of the website as needed.
- Native elements (`<button>`, `<a>` etc) were used where possible, such that their built-in functionality is preserved.
- Text content has a minimum contrast ration of 4.5:1 (some colors were changed from the design to meet this constraint).
- Alt text (with proper descriptions rather than "picture of car") has been added to all images.
- All interactive elements have (descriptive) labels.
- All interactive elements can be accessed via the keyboard, following an intuitive tab order.
- All interactive elements have clearly noticeable hover, active and focus states.
- Animations are disabled when `prefers-reduced-motion` is set.
- Semantic HTML elements (`<section>`, `<nav>` etc) were used where applicable.

While perfect accessibility is a difficult goal to reach, I think this resulted in a highly accessible end result.

## Responsiveness

The site is responsive over all real-world resolutions, although it won't be very usable on an Apple Watch. Breakpoints can be found in `src/breakpoints.scss`, and have been stolen from [Bootstrap 5](bootstrap).

Images were only provided in a 1400px resolution, so any screen sizes higher than that will start to look pixelated.

## Performance

As with most things web, better performance = fewer and smaller network requests.

All images are optimised for the web (using [sharp](sharp)), and smaller variants are provided for smaller resolutions. Configuration for this can be found in `sharp.config.json`. SVG images are used where possible, and inlined where possible to reduce HTTP requests.

There are also no analytics added, which tend to significantly increase bundle sizes.

HTML, CSS and JS files are minified, uglified and tree-shaken by Parcel. This results in a total download size of 700kb for desktop, and 350kb for mobile devices. In comparison, the [actual Model S](teslamodels) page is ~15MB, though that obviously includes significantly more functionality.

The amount of JS code is mininal both in scope and size, so actual performance (as in CPU cycles and memory usage) is also negligible. All animations are done via CSS, and by using properties that are known to animate well (such as `transform`).

## Software Engineering

I attempted to build this project as though it was a much larger project, structuring it to make it scale and be more robust. The folder structure is somewhat intuitive, proper technologies and tooling were used and the code written to be readable and extendable.

The SCSS code makes use of variables and has been split out into modules, the TypeScript code has been broken up into separate smaller functions and the HTML is lightly sprinked with comments.

## Browser Compatibility

This project is compatible with both IE11 and EdgeHTML, as requested. This was done by adding polyfills for missing JS functionality, and [autoprefixing](autoprefix) CSS statements to make them backwards compatible.

Browser compatibility is configured in the `package.json` via the `browserslist` field.

In addition, if JavaScript functionality is disabled (or missing) the page functions correctly and even provides the user with a table of ranges in place of the range calculator.

## Further Work

There is some things I would've like to do, but ended up not having the time for:

- Updating the TS code in `src/main.ts` to use less newer JS features (like `array.find`), allowing me to use less polyfills.
- Standardising the SCSS code a bit more by doing things like adding mixins (like for transitions), and building a proper theme (colors, font sizes etc).
- Writing unit tests for the TS with [Jest](jest). I wrote the TS such that it would be testable (minimal side effects from functions, no global variables etc), but that's as far as I got.
- Configuring proper linting, along with hooks to run the linter. I used [Prettier](prettier) along with the `.editorconfig` during development, but it would've been nice to add proper lint configuration and tooling.
- Setting up CI/CD pipelines via [GitLab CI](gitlabci) to lint and deploy the site automatically.
- Adding static site testing (via something like [Percy](percy)), so that changes can be reviewed and accepted in pull requests.

Thanks for taking a look!

[demo]: https://tesla.vanhattum.xyz
[firebase]: https://firebase.google.com
[firebase-cli]: https://github.com/firebase/firebase-tools
[typescript]: https://www.typescriptlang.org
[scss]: https://sass-lang.com
[parcel]: https://parceljs.org
[sharp]: https://sharp.pixelplumbing.com
[teslamodels]: https://www.tesla.com/en_eu/models
[jest]: https://jestjs.io
[percy]: https://percy.io
[gitlabci]: https://docs.gitlab.com/ee/ci/
[autoprefix]: https://parceljs.org/languages/css/#vendor-prefixing
[prettier]: https://prettier.io
[node]: https://nodejs.org/en/
[bootstrap]: https://getbootstrap.com/docs/5.2/layout/containers/#content
